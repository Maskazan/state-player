import { PlayerState } from "../types";

export const dummyState: PlayerState = {
  playing: false,
  locked: false,
  currTime: 0,
  playlist: [],
  currSong: -1
};

export default {
  playing: false,
  locked: false,
  currTime: 0,
  playlist: [
    {
      name: "Song 1",
      uri: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3"
    },
    {
      name: "Song 2",
      uri: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-2.mp3"
    },
    {
      name: "Song 3",
      uri: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-3.mp3"
    }
  ],
  currSong: 0
} as PlayerState;
