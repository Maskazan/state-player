import { Click } from '../types';
import { createAction } from '@reduxjs/toolkit';

export const lock = createAction<void>('lock');
export const play = createAction<void>('play');

export const next = createAction('next', (click: Click = Click.Single) => ({
  payload: undefined,
  meta: { click }
}));

export const prev = createAction('prev', (click: Click = Click.Single) => ({
  payload: undefined,
  meta: { click }
}));

export const updateCurrTime = createAction<number>('update-curr-time')

export type Action = ReturnType<
  typeof lock | typeof play | typeof next |
  typeof prev | typeof updateCurrTime
>