import 'antd/dist/antd.css'

import { Col, Layout, Row } from 'antd'

import React from 'react';
import StatePlayer from './components/StatePlayer'

const { Content } = Layout

function App() {
  return (
    <Layout>
      <Content style={{ padding: 150 }}>
        <StatePlayer reducerType="state-reducer" />
      </Content>
    </Layout>
  );
}

export default App;
