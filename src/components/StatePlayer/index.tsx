import { Button, Col, Divider, Progress, Row } from 'antd';
import {
  LockOutlined,
  PauseCircleOutlined,
  PlayCircleOutlined,
  StepBackwardOutlined,
  StepForwardOutlined,
  UnlockOutlined
} from '@ant-design/icons'
import React, { useEffect, useRef } from 'react';
import useReducer, {
  getCurrSongName,
  getCurrSongURI,
  getCurrTime,
  getProgress,
  isLocked,
  isPlaying,
  lock,
  next,
  play,
  prev,
  updateCurrTime
} from './state';

import { Click } from './types';

export type PlayerPropTypes = {
  reducerType: 'state-reducer' | 'classic-reducer'
}

const Player = ({ reducerType }: PlayerPropTypes): JSX.Element => {
  const [state, dispatch] = useReducer();

  const playing = isPlaying(state)
  const songURI = getCurrSongURI(state)
  const currTime = getCurrTime(state)

  const audioRef = useRef<HTMLAudioElement>(null);
  const { current: audio } = audioRef;

  // On song playing changed
  useEffect(() => {
    if (audio === null) {
      return;
    }
    playing ? audio.play() : audio.pause();
  }, [audio, playing]);

  // On song rewind
  useEffect(() => {
    if (audio !== null && Math.abs(audio.currentTime - currTime) > 1) {
      audio.currentTime = currTime
    }
  }, [currTime, audio])

  // On song uri changed
  useEffect(() => {
    if (playing && audio !== null) audio.play()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [audio, songURI])

  return (
    <div>
      <Divider>{getCurrSongName(state)} </Divider>
      <Row gutter={16}>
        <Progress percent={getProgress(state, audio)} showInfo={false} />
      </Row>
      <Row gutter={16} justify="center" align="middle">
        <Col span={6}>
          <Button
            onClick={() => dispatch(play())}
            icon={playing ? <PauseCircleOutlined /> : <PlayCircleOutlined />}
            block
          />
        </Col>
        <Col span={6}>
          <Button
            onClick={() => dispatch(prev(Click.Single))}
            onDoubleClick={() => dispatch(prev(Click.Double))}
            icon={<StepBackwardOutlined />}
            block
          />
        </Col>
        <Col span={6}>
          <Button
            onClick={() => dispatch(next(Click.Single))}
            onDoubleClick={() => dispatch(next(Click.Double))}
            icon={<StepForwardOutlined />}
            block
          />
        </Col>
        <Col span={6}>
          <Button
            onClick={() => dispatch(lock())}
            icon={isLocked(state) ? <LockOutlined /> : <UnlockOutlined />}
            block
          />
        </Col>
      </Row>
      <audio
        ref={audioRef}
        src={getCurrSongURI(state)}
        onTimeUpdate={e => dispatch(updateCurrTime((e.target as HTMLAudioElement).currentTime))}
      />
    </div>
  )
}

export default Player;
