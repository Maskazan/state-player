import { PlayerState, Song } from "../types";

export const getCurrSong = (state: PlayerState): Song =>
  state.playlist[state.currSong];

export const getCurrSongName = (state: PlayerState): string =>
  getCurrSong(state).name;

export const getCurrSongURI = (state: PlayerState): string =>
  getCurrSong(state).uri;

export const isPlaying = (state: PlayerState): boolean =>
  state.playing

export const isLocked = (state: PlayerState): boolean =>
  state.locked

export const getCurrTime = (state: PlayerState): number =>
  state.currTime

export const getProgress = (state: PlayerState, audio: HTMLAudioElement | null): number =>
  getCurrTime(state) / (audio?.duration || 0) * 100