export type ReducerType = 'LockedReducer' | 'ReadyReducer' | 'PlayingReducer'

export interface Song {
  name: string;
  uri: string;
}

export interface PlayerState {
  playing: boolean;
  locked: boolean;
  currTime: number;
  playlist: Song[];
  currSong: number;
}

export enum Click {
  Single = 0,
  Double = 1
}

export interface ReducerEnhancer {
  curr: ReducerType
  prev: ReducerType
}

export interface ActionEnhancer {
  meta: { type: ReducerType }
}
