import { ActionEnhancer, Click, PlayerState, ReducerEnhancer, ReducerType } from '../types'
import { AnyAction, createReducer } from '@reduxjs/toolkit'
import { lock, next, play, prev, updateCurrTime } from './actions'

import { dummyState } from './initial-state'

type Reducer = (state: PlayerState, action: AnyAction) => PlayerState

const reducers: {
  [type in ReducerType]?: Reducer
} & ReducerEnhancer = {
  curr: 'ReadyReducer',
  prev: 'ReadyReducer'
}

const switchTo = (reducerState: ReducerType | 'prev', reducer: Reducer): Reducer => 
  (state: PlayerState, action: AnyAction): PlayerState => {
    const { prev, curr } = reducers

    if (curr === (action as AnyAction & ActionEnhancer).meta.type) {
      reducers.prev = reducers.curr
      reducers.curr = reducerState === 'prev' ? prev : reducerState
    }

    return reducer(state, action)
  }

const update = (prop: keyof PlayerState, value: PlayerState[typeof prop]) =>
  (state: PlayerState): PlayerState =>
    ({ ...state, [prop]: value })

const updateCurrTimeReducer = (
  state: PlayerState,
  action: ReturnType<typeof updateCurrTime>
): PlayerState => ({
  ...state,
  currTime: action.payload
})

const nextSongReducer = (state: PlayerState): PlayerState => {
  const { currSong, playlist } = state
  return {
    ...state,
    currSong: currSong === playlist.length - 1 ? 0 : currSong + 1
  }
}

const prevSongReducer = (state: PlayerState): PlayerState => {
  const { currSong, playlist } = state
  return {
    ...state,
    currSong: currSong === 0 ? playlist.length - 1 : currSong - 1
  }
}

reducers.LockedReducer = createReducer<PlayerState>(dummyState, builder =>
  builder
    .addCase(lock, switchTo('prev', update('locked', false)))
    .addCase(updateCurrTime, updateCurrTimeReducer)
);

reducers.ReadyReducer = createReducer<PlayerState>(dummyState, builder =>
  builder
    .addCase(lock, switchTo('LockedReducer', update('locked', true)))
    .addCase(play, switchTo('PlayingReducer', update('playing', true)))
    .addCase(prev, prevSongReducer)
    .addCase(next, nextSongReducer)
    .addCase(updateCurrTime, updateCurrTimeReducer)
);

reducers.PlayingReducer = createReducer<PlayerState>(dummyState, builder =>
  builder
    .addCase(lock, switchTo('LockedReducer', update('locked', true)))
    .addCase(play, switchTo('ReadyReducer', update('playing', false)))
    .addCase(prev, (state, action) => {
      if (action.meta.click === Click.Single) {
        return {
          ...state,
          currTime: state.currTime - 5
        }
      }

      return prevSongReducer(state)
    })
    .addCase(next, (state, action) => {
      if (action.meta.click === Click.Single) {
        return {
          ...state,
          currTime: state.currTime + 5
        }
      }

      return nextSongReducer(state)
    })
    .addCase(updateCurrTime, updateCurrTimeReducer)
);

export default reducers as {
  [type in ReducerType]: Reducer
} & ReducerEnhancer
