import { ActionEnhancer, PlayerState } from '../types'
import { useCallback, useReducer } from 'react'

import { Action } from './actions'
import initialState from './initial-state'
import reducers from './reducers'

type EnhancedAction = Action & ActionEnhancer

const reducer = (state: PlayerState, action: EnhancedAction): PlayerState => {
  return reducers[action.meta.type](state, action)
}

export default (): [PlayerState, (action: Action) => void] => {
  const [state, dispatch] = useReducer(
    reducer,
    initialState
  )

  const enhancedDispatch = useCallback((action: Action): void =>
    dispatch({
      ...action,
      meta: {
        ...(action as EnhancedAction).meta,
        type: reducers.curr
      }
    }), [])

  return [state, enhancedDispatch]
}

export * from './actions'
export * from './selectors'
